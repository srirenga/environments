# == Class: infra::java
#
class infra::java (
  $ensure = present,
  $java_version = openjdk-8-jdk,
)
  {
  package { $java_version:
    ensure => $ensure,
    #name   => $java_version,
  }
}
